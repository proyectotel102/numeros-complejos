 #include <iostream>
 #include <cmath>
 #define PI 3.14159265

using namespace std;

class NumeroComplejo {
    
    public:
        double real, imaginario;
        double realx, imaginariox;
        
        void suma(NumeroComplejo a, NumeroComplejo b) {
            
            double real = a.real + b.real;
            double imaginario = a.imaginario + b.imaginario;
            NumeroComplejo z = NumeroComplejo(real, imaginario);
            if(z.imaginario == 0){
                cout << z.real << endl;
            } else if (z.real == 0){
                if (z.imaginario == 1){
                cout << "i" << endl;
            } else if (z.imaginario == -1){
                cout << "-i" << endl;
            } else {
            cout << z.imaginario << "i" << endl;            
            }
            } else {
            if ( z.imaginario <= -2 ) {
            cout << z.real << " - " << (-z.imaginario) << "i" << endl;
            } else if ( z.imaginario == -1 ) {
            cout << z.real << " - i" << endl;
            } else if ( z.imaginario == 1 ) {
            cout << z.real << " + i" << endl;
            }  else {
            cout << z.real << " + " << z.imaginario << "i" << endl;
            }
            }
        }
         
        void resta(NumeroComplejo a, NumeroComplejo b) {
            
            double real = a.real - b.real;
            double imaginario = a.imaginario - b.imaginario;
            NumeroComplejo z = NumeroComplejo(real, imaginario);
            if(z.imaginario == 0){
                cout << z.real << endl;
            } else if (z.real == 0){
                if (z.imaginario == 1){
                cout << "i" << endl;
            } else if (z.imaginario == -1){
                cout << "-i" << endl;
            } else {
            cout << z.imaginario << "i" << endl;            
            }
            } else {
            if ( z.imaginario <= -2 ) {
            cout << z.real << " - " << (-z.imaginario) << "i" << endl;
            } else if ( z.imaginario == -1 ) {
            cout << z.real << " - i" << endl;
            } else if ( z.imaginario == 1 ) {
            cout << z.real << " + i" << endl;
            }  else {
            cout << z.real << " + " << z.imaginario << "i" << endl;
            }
            }
        }
         
        void multiplicacion(NumeroComplejo a, NumeroComplejo b) {
            
            double real = a.real * b.real - a.imaginario * b.imaginario;
            double imaginario = a.imaginario * b.real + a.real * b.imaginario;
            NumeroComplejo z = NumeroComplejo(real, imaginario);
            if(z.imaginario == 0){
                cout << z.real << endl;
            } else if (z.real == 0){
                if (z.imaginario == 1){
                cout << "i" << endl;
            } else if (z.imaginario == -1){
                cout << "-i" << endl;
            } else {
            cout << z.imaginario << "i" << endl;            
            }
            } else {
            if ( z.imaginario <= -2 ) {
            cout << z.real << " - " << (-z.imaginario) << "i" << endl;
            } else if ( z.imaginario == -1 ) {
            cout << z.real << " - i" << endl;
            } else if ( z.imaginario == 1 ) {
            cout << z.real << " + i" << endl;
            }  else {
            cout << z.real << " + " << z.imaginario << "i" << endl;
            }
            }
            
        }
        void division(NumeroComplejo a, NumeroComplejo b) {
                
            double real = (a.real * b.real + a.imaginario * b.imaginario) / (b.real * b.real + b.imaginario * b.imaginario);
            double imaginario = (a.imaginario * b.real - a.real * b.imaginario) / (b.real * b.real + b.imaginario * b.imaginario);
            cout.precision(2);
            NumeroComplejo z = NumeroComplejo(real, imaginario);
            if(z.imaginario == 0){
                cout << "i" << endl;
            } else if (z.real == 0){
                if (z.imaginario == 1){
                cout << "i" << endl;
            } else if (z.imaginario == -1){
                cout << "-i" << endl;
            } else {
            cout << z.imaginario << "i" << endl;            
            }
            } else {
            if ( a.imaginario || b.imaginario <= -2 ) {
            cout << z.real << " - " << (z.imaginario) << "i" << endl;
            } else if ( z.imaginario == -1 ) {
            cout << z.real << " - i" << endl;
            } else if ( z.imaginario == 1 ) {
            cout << z.real << " + i" << endl;
            }  else {
            cout << z.real << " + " << z.imaginario << "i" << endl;
            }
            }
        }
        void polar(NumeroComplejo a, NumeroComplejo b){

            double real = sqrt(a.real*a.real + a.imaginario*a.imaginario);
            double imaginario = atan(a.imaginario/a.real) * 180/PI;
            cout.precision(4);
            NumeroComplejo z = NumeroComplejo(real, imaginario);
            cout <<"\nPolar del primer complejo: " << endl;
        
              if(a.real < 0){
                cout << z.real << "[cos(" << z.imaginario + 180 << "°)+isin(" << z.imaginario + 180 << "°)]" << endl;
              } else if (a.imaginario < 0){
                cout << z.real << "[cos(" << z.imaginario + 360 << "°)+isin(" << z.imaginario + 360 << "°)]" << endl;
            } else {
                    cout << z.real <<"[cos(" << z.imaginario << "°)+isin(" << z.imaginario << "°)]" << endl;
            }
            double realx = sqrt(b.real*b.real + b.imaginario*b.imaginario);
            double imaginariox = atan(b.imaginario/b.real) * 180/PI;
            cout.precision(4);
            NumeroComplejo x = NumeroComplejo(realx, imaginariox);
            cout <<"\nPolar del segundo complejo: " << endl;
            if(b.real < 0){
                cout << x.real << "[cos(" << z.imaginario + 180 << "°)+isin(" << z.imaginario + 180 << "°)]" << endl;
            } else if (b.imaginario < 0){
                cout << x.real << "[cos(" << x.imaginario + 360 << "°)+isin(" << x.imaginario + 360 << "°)]" << endl;
            } else {
                cout << x.real <<"[cos(" << x.imaginario << "°)+isin(" << x.imaginario << "°)]" << endl;
            }
        
        }
            //El constructor
        NumeroComplejo(double real, double imaginario) {
        this->real = real;
        this->imaginario = imaginario;
    }
};

int main() {
    
    double realA;
    double imaginarioA;
    double realB;
    double imaginarioB;

    //input del usuario
    cout <<"Ingrese datos del primer numero complejo" << endl;
    cout <<"\nParte real: "; cin >> realA;
    cout <<"Parte imaginaria: "; cin >> imaginarioA;
    
    cout <<"\nIngrese datos del segundo numero complejo";
    cout <<"\n\nParte real: "; cin >> realB;
    cout <<"Parte imaginaria: "; cin >> imaginarioB;
    
    float opcion;
    cout <<"\n1 Suma, 2 Resta, 3 Multiplicacion, 4 División, 5 Polar "; cin >> opcion;
                
    NumeroComplejo a = NumeroComplejo(realA, imaginarioA);
    NumeroComplejo b = NumeroComplejo(realB, imaginarioB);

    //opciones a elegir 
    if( opcion == 1 ){
    a.suma(a, b); 
    }
    else if ( opcion == 2 ) {
    a.resta(a, b);
    }
    else if ( opcion == 3 ) {
    a.multiplicacion(a, b);
    }
    else if ( opcion == 4 ) {
    a.division(a, b);
    }
    else if ( opcion == 5) {
        a.polar(a,b);
    }
    else {
        cout <<"Ingrese una opcion valida " << endl;
    }
        char volver;
    cout << "\n¿Desea realizar otra operación? (s/n) "; cin >> volver;
            if (volver == 's'){
                return main();
            }else{
                cout <<"Calculadora finalizada con éxito" << endl;exit;
            }
    return 0;
}
