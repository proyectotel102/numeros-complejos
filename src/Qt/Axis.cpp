QValueAxis *angularAxis = new QValueAxis();
angularAxis->setTickCount(9); //Se coloca en angulos  de 0-360
angularAxis->setLabelFormat("%.1f");
angularAxis->setShadesVisible(true);
angularAxis->setShadesBrush(QBrush(QColor(269, 289, 212)));
chart->addAxis(angularAxis, QPolarChart::PolarOrientationAngular);

QValueAxis *radialAxis = new QValueAxis();
radialAxis->setTickCount(8);
radialAxis->setLabelFormat("%d");
chart->addAxis(radialAxis, QPolarChart::PolarOrientationRadial);
