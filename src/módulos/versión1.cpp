#include <iostream>

using namespace std;

int main() {
    cout <<"Calculadora de numeros complejos" << endl;
    cout <<"\nIngrese datos del primer numero complejo: " << endl;
    
    int real1{};
    cout <<"Parte real: "; cin >> real1;
    int imaginaria1{};
    cout <<"Parte imaginaria: "; cin >> imaginaria1;
    
    cout <<"\t\nIngrese datos del segundo numero complejo: " << endl;
    int real2{};
    cout <<"Parte real: "; cin >> real2;
    int imaginaria2{};
    cout <<"Parte imaginaria: "; cin >> imaginaria2;
    
    int opcion{};
    
    cout <<"\n1 Suma "; cin >> opcion;
    
    int real_resultado{};
    int imaginario_resultado{};
    
    if ( opcion == 1 ) {
    real_resultado = real1 + real2;
    imaginario_resultado = imaginaria1 + imaginaria2;
  }  else {
    return 0;
  }
  
  return 0;
}
