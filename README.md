# Números complejos 
Calucadora para realizar operaciones básicas con números complejos

#### Fecha de entrega: 10/12/2022

#### Integrantes

María Fernanda Hancco

Rol: xxxxxxxxxxxx

Matías Perelli

Rol: xxxxxxxxxxxx


# Descripción y avances
### Introducción

Nuestro poblema físico-matematico entra dentro de la rama de los números complejos, es decir, como se operan estos tipos de números y la introducción del concepto "i". 

El número "i" dentro del conjunto de números complejos se refiere al número raiz de menos uno, lo interesante aquí es de qué manera, esta peculiar letra se verá implementada en un programa de C++ y cómo manera se llevará a cabo.

La forma en que llevamos a cabo nuestra solución es gracias a nuestro conocimiendo de las algebras básicas para desarrollar ejercicios de números complejos, la exigencia dentro de estre proyecto es muy alta debido a la cantidad de operaciones necesarias para poder entregar un solo resultado, esto sumado a que como grupo somos nuevos en programación dentro de C++

## Objetivos.
1. **General**: El usuario será capaz de obtener un resultado correcto de la operación deseada.
    - **Especificos:**
        1. Utilizar las librerias correspondientes para que al momento del cálculo no ocurran errores.
        2. Depurar el código de tal manera que el usuario que se interese en mejorarlo, pueda entender cómo funciona.
        3. Identificar erroes en los signos y al momento de trabajar con el "i" para poder arreglarlos.

2. **General**: El usuario recibirá un gráfico del resultado y la calculadora será visual.
    - **Especificos.**
        1. Aprender todo sobre la utilización de QtCreator y desarrollar los gráficos.
        2. Comprobar la efectividad de los gráficos.
        3. Desarrollar la calculadora visualmente atractiva y funcional.

### Desarrollo 

Logramos solucionar nuestro problema matematico mediante el uso principalmente de librerias que nos entrega c++, con esta ayuda es como fuimos desarrollando paso a paso nuestra calculadora. El código funciona de la siguiente manera: al momento de ejecutar (ver Instalación) nuesto programa, se inicia inmediatamente el input el cual le pregunta al usuario qué números desea ingresar, siempre y cuando estos numeros sean reales, no hay ningún problema.

Escogimos esta solución porque nos parece la manera más sencilla de trabajar con números complejos, el hecho de ingresar los datos que el usuario desee, junto con la operación matemática que se esté buscando, hacen que al momento de entregar nuestra respuesta, se vea todo mucho más simple.

Las ventajas que tiene es que no hay problemas con los signos, el usuario solo debe de ingresar los números y el programa los arreglará de manera autonoma. Una de las grandes fallas que no pudimos mejorar fue el hecho de depurar el código, esto ocurrió por nuestro poco manejo en C++ 
### Conclusión

Luego de entender como funciona nuestro programa y saber ejecutarlo, no queda más que dar la bienvenida a todo aquel que desee probarlo.

En entregas futuras nuestro mayor deseo es que el código sea lo más corto y entendible para los usuarios.

# Diagrama de componentes

[![itsmedio-drawio.png](https://i.postimg.cc/fTNgGyWx/itsmedio-drawio.png)](https://postimg.cc/mhmwMbHD)

## Requisitos de Compilación o Plataforma.
Programa creado a partir de sistema operativo Linux, es compatible con MacOS y Windows.                                        
Para su ejecución es necesario tener instalado un compilador de C++ y un visualizador del gráfico, en este caso cualquier software de QtCharts.
## Instalación.
Una demostración básica de como compilar y ejecutar nuestro programa

**Ejemplo:**
1. Descargar el repositorio

2. Abrir una terminal en Linux
3. Dirigirse al directorio donde se encuentre el codigo
4. Compilar ejecutando los siguientes comandos
    ~~~
    g++ complejos.cpp -o imprimir.out
    ./imprimir.out
    ~~~
## Referencias

- [ ] Complex variables and applications / Churchill, Ruel Vance Editorial
- [ ] The C++programming language / Bjarne Stroustrup Stroustrup, Bjarne
- [ ] El lenguaje unificado de modelado: manual de referencia / Rumbaugh, James
- [ ]  Beginning Linux programming / Matthew, Neil
